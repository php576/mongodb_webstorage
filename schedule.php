<?php
    require_once "conn.php";
    
    $query = array();

    if (array_key_exists("gr_id", $_GET)) {
        $query = ["type" => "Laboratory", 
            "groups" => array('$regex' => $_GET['gr_id'])];
    } else if (array_key_exists("tch_id", $_GET)
              && array_key_exists("sbj_id", $_GET)) {
        $query = ["type" => "Lecture",
                  "teachers" => array('$regex' => $_GET['tch_id']),
                  "sbj" => (int) $_GET['sbj_id']
                 ];
    } else if (array_key_exists("aud_id", $_GET)) {
        $query = ["auditorium" => (int) $_GET['aud_id'] ];
    }

    $cursor = $db->lessons->find($query);
    $data = $cursor->toArray();    

    header('Content-Type:application/json');
    echo json_encode($data);
