let audId = localStorage.getItem('aud_id');
let audName = auditoriums[audId];
$("#lbAudName").text(audName);

let btnUpdate = $("#btnUpdate");
let savedValue = localStorage.getItem(audName); 

let lbUploadDate = $("#lbUploadDate"); 
if (!savedValue){
    lbUploadDate.text("Расписание не обнаружено в локальном хранилище.");
    btnUpdate.text("Загрузить");
} else {
    lbUploadDate.text("Расписание было загружено");
    let date = "";
    try {
        let parsed = JSON.parse(savedValue);  
        date = parsed.date;
        JSON.parse(parsed.lessons);
    } catch(e) {
        lbUploadDate.append(", но не может быть обработано.");
        date = "";
    }
    lbUploadDate.append(date);
    if ("" != date) {
        updatePage();   
    }
}
btnUpdate.click( () => {
    let ajax = new XMLHttpRequest();
    ajax.open('GET', './schedule.php?aud_id=' + audId);
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4 && ajax.status == 200) {
            if (!(localStorage.getItem(audName) === undefined)) {
                btnUpdate.text("Обновить");
            }
            
            let uploadDate = new Date().toLocaleString();
            localStorage.setItem(
                audName, JSON.stringify(
                    { 
                        date: uploadDate,
                        lessons: ajax.responseText
                    }       )
            );
            updatePage();
        }
    };
    ajax.send(null);
});

function updatePage() {
    let new_tbody = document.createElement('tbody');
    
    let saved = localStorage.getItem(audName);
    let parsed = JSON.parse(saved);
    let lessons = JSON.parse(parsed.lessons);
    for (lesson of lessons) {
        let row = new_tbody.insertRow(-1);
        row.insertCell(0).innerHTML = teachers[lesson.teachers];
        row.insertCell(1).innerHTML = lesson.date;
        row.insertCell(2).innerHTML = lesson.les_num;
        row.insertCell(3).innerHTML = subjects[lesson.sbj];
        row.insertCell(4).innerHTML = lesson.type;
        let arr_groups = lesson.groups.split(',');
        let str_groups = "";
        for (let i = 0; i < arr_groups.length - 1; ++i) {
            str_groups += groups[parseInt(arr_groups[i])] + ', ';
        }
        str_groups += groups[
            parseInt(arr_groups[arr_groups.length - 1])
        ];
        row.insertCell(5).innerHTML = str_groups;
    }
    if (lessons.length == 0) {
        $('#lbEmpty').text(
            "Пока нет ни одного занятия в расписании.");
    } else {
        $('#lbEmpty').text("");        
    }
    
    $("#lbUploadDate").text(
        "Расписание было загружено " + parsed.date);
    
    let table = document.getElementById("audSchTable");
    let old_tbody = table.firstChild;
    old_tbody.parentNode.replaceChild(new_tbody, old_tbody);
}

