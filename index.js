function initSelect(id, key) {
    let select = document.getElementById(id);
    let savedValue = localStorage[key];
    if ("undefined" == savedValue || undefined === savedValue) {
        localStorage.setItem(key, select.value);
    } else {
        select.value = savedValue;
    }
    select.onchange = () => localStorage.setItem(key, select.value);
}

initSelect('groups', 'gr_id');
initSelect('teachers', 'tch_id');
initSelect('subjects', 'sbj_id');
initSelect('auditoriums', 'aud_id')

class Tuple {
    constructor(_id, name) {
        this._id = _id;
        this.name = name;
    }
}

function parseSelect(sel_id) {
    let sel = document.getElementById(sel_id);
    let options = sel.children;
    let arr = new Array();
    for (var i=0, opt; opt=options[i]; i++) {
        let _id = opt.getAttribute('value');
        let name = opt.innerHTML;
        arr.push(new Tuple(_id, name));
    }
    return JSON.stringify(arr);
}

localStorage.setItem('groups', parseSelect('groups'));
localStorage.setItem('teachers', parseSelect('teachers'));
localStorage.setItem('subjects', parseSelect('subjects'));
localStorage.setItem('auditoriums', parseSelect('auditoriums'));