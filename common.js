function getArray(key) {

let parsed = JSON.parse(
    localStorage.getItem(key)
);    
    let array = new Array();
    for (tuple of parsed) {
        array[tuple._id] = tuple.name;
    }
    return array;
}

let groups = getArray('groups');
let teachers = getArray('teachers');
let auditoriums = getArray('auditoriums'); 
let subjects = getArray('subjects');
