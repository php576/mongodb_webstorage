<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Shcedule</title>
    <link href="style.css" rel="stylesheet">
    <script src="index.js" defer></script>
</head>
<body>
   <h1>Расписание</h1>
        <label for="groups">Выберите группу:</label>
        <select id="groups">
    <?php  
        require_once "conn.php";
            
        $cursor = $db->groups->find();
        
        foreach ($cursor as $group) {
            echo "<option value='${group['_id']}'>${group['title']}</option>";
        }
    ?>
        </select>
        <br>
        <a href="group.htm">
            <button>
                Показать лабораторные работы   
            </button>
        </a>
             
    <hr>
    
        <label for="teachers">Преподаватель:</label>
        <select id="teachers" name="tch_id">
    <?php
        $cursor = $db->teachers->find();
        
        foreach ($cursor as $tch) {
            echo "<option value='${tch['_id']}'>${tch['name']}</option>";
        }
    ?>
        </select>
        <br>
        <label for="subjects">Предмет:</label>
        <select id="subjects" name="sbj_id">
            <?php
                $cursor = $db->subjects->find();
        
                foreach ($cursor as $sbj) {
                    echo "<option value='${sbj['_id']}'>${sbj['name']}</option>";
                }
            ?>
        </select>
        <br>
        <a href="tch.htm">
            <button>Показать расписание лекций</button>
        </a>
    <hr>
    
        <label for="auditoriums">
            Выберите аудиторию:
        </label>
        <select id="auditoriums" name="aud">
            <?php
                $cursor = $db->auditoriums->find();
        
                foreach ($cursor as $aud) {
                    echo "<option value='${aud['_id']}'>${aud['name']}</option>";
                }            
            ?>
        </select>
        <br>
        <a href="aud.htm">
            <button>Показать</button>
        </a>
    <br>
</body>
</html>