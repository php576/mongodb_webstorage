function updatePage() {
    let new_tbody = document.createElement('tbody');
    
    let saved = localStorage.getItem(groupName);
    let parsed = JSON.parse(saved);
    let lessons = JSON.parse(parsed.lessons);
    for (lesson of lessons) {
        let row = new_tbody.insertRow(-1);
        row.insertCell(0).innerHTML = lesson.date;
        row.insertCell(1).innerHTML = lesson.les_num;
        row.insertCell(2).innerHTML = auditoriums[lesson.auditorium];
        row.insertCell(3).innerHTML = subjects[lesson.sbj];
        row.insertCell(4).innerHTML = teachers[lesson.teachers];
    }
    if (lessons.length == 0) {
        document.getElementById('lbEmpty')
            .innerHTML = "Пока нет ни одной лаборатоной в расписании.";
    }
    document.getElementById("lbUploadDate")
        .innerHTML = "Расписание было загружено " + parsed.date;

    let table = document.getElementById('tbSchedule');
    let old_tbody = table.firstChild;
    old_tbody.parentNode.replaceChild(new_tbody, old_tbody);
}

if (localStorage.getItem(groupName)) {
    updatePage();
}

document.getElementById("btnUpdate")
    .onclick = () => {
    let ajax = new XMLHttpRequest();
    ajax.open('GET', './schedule.php?gr_id='
              + localStorage.getItem('gr_id'));
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4 && ajax.status == 200) {
            if (!(localStorage.getItem(groupName) === undefined)) {
                document.getElementById("btnUpdate")
                    .innerHTML = "Обновить";
            }

            let uploadDate = new Date().toLocaleString();
            localStorage.setItem(
                groupName, JSON.stringify(
                    { 
                        date: uploadDate,
                        lessons: ajax.responseText
                    }       )
            );
            updatePage();
        }
    };
    ajax.send(null);
};